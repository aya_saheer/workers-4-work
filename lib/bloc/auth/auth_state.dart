abstract class AuthState{}
class Loading{}

class Authenticated{}

class Unauthenticated{}

abstract class AuthError extends AuthState {}

class UserAlreadyExist extends AuthError {}

class UnmatchedUser extends AuthError {}

class UnmatchedPassword extends AuthError {}

class NameFieldRequired extends AuthError {}

class EmailFieldRequired extends AuthError {}

class PasswordFieldRequired extends AuthError {}

class PhoneNumberFieldRequired extends AuthError {}

class CityFieldRequired extends AuthError {}

class WorkFieldRequired extends AuthError {}

class SpecializationFieldRequired extends AuthError {}

class InvalidEmailFormat extends AuthError {}
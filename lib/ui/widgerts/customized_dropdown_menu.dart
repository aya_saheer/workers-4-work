import 'package:flutter/material.dart';

class CustomizedDropDownMenu extends StatefulWidget {
  final List<DropdownMenuItem<String>> items;
  final String hint;
  String value;
  CustomizedDropDownMenu({this.items, this.hint});

  @override
  _CustomizedDropDownMenuState createState() => _CustomizedDropDownMenuState();
}

class _CustomizedDropDownMenuState extends State<CustomizedDropDownMenu> {
  @override
  Widget build(BuildContext context) => Padding(
    padding: EdgeInsets.only(bottom: 10),
    child: Container(
      decoration:ShapeDecoration(
        shape: RoundedRectangleBorder(
          side: BorderSide(width: 1, style: BorderStyle.solid, color: Color(0xff969696)),
          borderRadius: BorderRadius.all(Radius.circular(25)),
        ),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: DropdownButton(
            value: this.widget.value,
            hint: Text(this.widget.hint),
            onChanged: (String newValue){
              setState(() {
                this.widget.value = newValue;
              });
            },
            items: this.widget.items,
          ),
        ),
      ),
    )
  );
}
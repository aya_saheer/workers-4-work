import 'package:flutter/material.dart';

class CustomizedTextField extends StatelessWidget {
  final String title;
  TextEditingController inputController;
  String Function(String) validator;
  final TextInputType keyboardType;
  bool hide;
  CustomizedTextField({this.title, this.hide = false, this.keyboardType, this.validator, this.inputController});

  @override
  Widget build(BuildContext context) => Padding(
    padding: EdgeInsets.only(bottom: 10),
    child: TextFormField(
      decoration: InputDecoration(
        labelText: this.title,
        contentPadding: EdgeInsets.all(12),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(25.0),
          borderSide: BorderSide()
        )
      ),
      keyboardType: this.keyboardType,
      obscureText: this.hide,
    ),
  );
}
import 'package:flutter/material.dart';

class CustomizedButton extends StatefulWidget {
  final String title;
  final EdgeInsetsGeometry padding;
  const CustomizedButton({this.title, this.padding});

  @override
  _CustomizedButtonState createState() => _CustomizedButtonState();
}

class _CustomizedButtonState extends State<CustomizedButton> {
  @override
  Widget build(BuildContext context) => Padding(
    padding: this.widget.padding,            
    child: SizedBox(
      width: MediaQuery.of(context).size.width,
      height: 45,
      child: RaisedButton(
        child: Text(this.widget.title, style: TextStyle(color: Colors.white)),
        color: Color(0xff345DA7),
        shape: StadiumBorder(),
        onPressed: () {},
      ),
    ),
  );
}
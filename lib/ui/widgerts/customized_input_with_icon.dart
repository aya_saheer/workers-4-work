import 'package:first_flutter_app/ui/widgerts/customized_text_filed.dart';
import 'package:flutter/material.dart';

class CustomizedInputWithIcon extends StatelessWidget {
  final String title;
  final IconData icon;
  final TextInputType keyboardType;
  TextEditingController inputController;
  String Function(String) validator;
  bool hide;
  CustomizedInputWithIcon({this.icon, this.title, this.hide = false, this.keyboardType, this.validator, this.inputController});

  @override
  Widget build(BuildContext context) => Row(
    children: [
      Icon(
        this.icon,
        color: Color(0xfffdd036)
      ),
      Expanded(
        child: CustomizedTextField(title: this.title, hide: this.hide, keyboardType: this.keyboardType, validator: this.validator, inputController: this.inputController)
      )
    ],
  );
}
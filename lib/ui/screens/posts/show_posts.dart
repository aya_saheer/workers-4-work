import 'package:flutter/material.dart';

enum Priority {
  urgent,
  important,
  medium,
  useless,
}

class DummyTask {
  final String text;
  bool isChecked;
  final Priority priority;

  Color getColor() => {
        Priority.useless: Colors.grey,
        Priority.medium: Colors.green,
        Priority.important: Colors.yellow,
        Priority.urgent: Colors.red,
      }[this.priority];

  DummyTask({
    @required this.text,
    this.isChecked = false,
    this.priority = Priority.medium,
  });
}

class ShowPosts extends StatelessWidget {

  final tasks = [
    DummyTask(text: "Dlkjdsfdsf sdf lkjaf lksaj flksjadf lkasjflkjsadf klasjdfkl asjdflk jasldkfjalskdfjlksjdflk jadsk lj"),
    DummyTask(text: "sdfxc sdf", isChecked: true),
    DummyTask(text: "qwe sdf", isChecked: true, priority: Priority.urgent),
    DummyTask(text: "asd sdf", priority: Priority.useless),
  ];

  @override
  Widget build(BuildContext context) => ListView(
          children: [
            ...tasks.map(
              (task) => Card(
                key: Key(task.text),
                elevation: 4,
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          CircleAvatar(
                            backgroundColor: Color(0xff345DA7), 
                            child: Text(''),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 10),
                            child: Text('Ahmed Ali')
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 30),
                            child: Text('Thursday, 2019.01.01')
                          )
                        ]
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text('Specialization'),
                            Text('Location')
                          ]
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 10),
                        child: Text('Nullam vulputate lorem ut leo. Sed volutpat. Etiam non pede. Nullam et mauris.'),
                      ),
                      Container(
                        height: 80,
                        width: MediaQuery.of(context).size.width,
                        color: Colors.grey,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          IconButton(
                            icon: Icon(Icons.comment),
                            color: Colors.grey,
                            iconSize: 20,
                            onPressed: () {},
                          ),
                          Text('24 Comments')
                        ],
                      )
                    ]
                  ),
                )
              ),
            ),
          ],
        );
}
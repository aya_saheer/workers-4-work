import 'package:first_flutter_app/ui/screens/posts/create_post.dart';
import 'package:first_flutter_app/ui/screens/posts/show_posts.dart';
import 'package:first_flutter_app/ui/screens/profile/customer/profile.dart';
import 'package:first_flutter_app/ui/screens/settings.dart';
import 'package:first_flutter_app/ui/screens/show_workers.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatefulWidget {

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _currentIndex = 0;

  final _bottomNavigationBarItems = [
    BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('')),
    BottomNavigationBarItem(icon: Icon(Icons.group), title: Text('')),
    BottomNavigationBarItem(icon: Icon(Icons.edit), title: Text('')),
    BottomNavigationBarItem(icon: Icon(Icons.person), title: Text('')),
    BottomNavigationBarItem(icon: Icon(Icons.settings), title: Text('')),
  ];

  final _bottomNavigationBarPages = [
    ShowPosts(),
    ShowWorkers(),
    CreatePost(),
    CustomerProfile(),
    Settings()
  ];

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(
        title: Text('Main Screen'),
        backgroundColor: Color(0xff345DA7),
        actions: [
          IconButton(
            tooltip: 'search',
            icon: Icon(Icons.search),
            onPressed: (){},
          ),

        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: _bottomNavigationBarItems,
        currentIndex: _currentIndex,
        selectedItemColor: Color(0xfffdd036),
        type: BottomNavigationBarType.fixed,
        onTap: (int index){
          setState((){
            _currentIndex = index;
          });
        },
      ),
      body: _bottomNavigationBarPages[_currentIndex]
    );
}
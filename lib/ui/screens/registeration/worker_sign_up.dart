import 'package:first_flutter_app/ui/widgerts/customized_button.dart';
import 'package:first_flutter_app/ui/widgerts/customized_dropdown_menu.dart';
import 'package:first_flutter_app/ui/widgerts/customized_text_filed.dart';
import 'package:flutter/material.dart';

class WorkerSignUp extends StatelessWidget {
  static const cities = [
    'Tripoli',
    'Benghazi',
    'Misrate'
  ];

  static const works = [
    'Medical',
    'Engineering',
    'IT',
    'Other'
  ];

  static const specializations = [
    'Web Developer',
    'Mobile Developer',
    'Doctor'
  ];

  final nameInputController = TextEditingController();
  final emailInputController = TextEditingController();
  final passwordInputController = TextEditingController();
  final phoneNumberInputController = TextEditingController();

  final List<DropdownMenuItem<String>> _cityOptions = cities.map(
    (String value) => DropdownMenuItem<String>(
      value: value,
      child: Text(value)
    )
  ).toList();

  final List<DropdownMenuItem<String>> _workOptions = works.map(
    (String value) => DropdownMenuItem<String>(
      value: value,
      child: Text(value)
    )
  ).toList();

  final List<DropdownMenuItem<String>> _specialzationOptions = specializations.map(
    (String value) => DropdownMenuItem<String>(
      value: value,
      child: Text(value)
    )
  ).toList();

  @override
  Widget build(BuildContext context) => SingleChildScrollView(
      child: Center(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 30, horizontal: 0),
              child:  FlutterLogo(
                size: 100
              ),
            ),
            CustomizedTextField(title: 'Name', keyboardType: TextInputType.text, inputController: this.nameInputController),
            CustomizedTextField(title: 'E-mail', keyboardType: TextInputType.emailAddress, inputController: this.emailInputController),
            CustomizedTextField(title: 'Password', keyboardType: TextInputType.text, inputController: this.passwordInputController, hide: true),
            CustomizedTextField(title: 'Phone Number', keyboardType: TextInputType.phone, inputController: this.phoneNumberInputController),
            CustomizedDropDownMenu(items: _cityOptions, hint: 'City'),
            CustomizedDropDownMenu(items: _workOptions, hint: 'Work'),
            CustomizedDropDownMenu(items: _specialzationOptions, hint: 'Specialization'),
            CustomizedButton(title: 'Sign Up', padding: EdgeInsets.only(bottom: 10))
          ],
        ),
      )
    ),
  );
}
import 'package:first_flutter_app/ui/screens/registeration/customer_sign_up.dart';
import 'package:first_flutter_app/ui/screens/registeration/worker_sign_up.dart';
import 'package:flutter/material.dart';

class SignUp extends StatelessWidget {
  final _tabs = [
    Tab(text: 'Customer'),
    Tab(text: 'Worker')
  ];

  final _pages = [
    CustomerSignUp(),
    WorkerSignUp()
  ];
  @override
  Widget build(BuildContext context) => DefaultTabController( 
    child: Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50),
        child: AppBar(
          backgroundColor: Color(0xff345DA7),
          bottom: TabBar(
            tabs: _tabs,
            indicatorColor: Color(0xfffdd036),
          ),
        )
      ),
      body: TabBarView(
        children: _pages,
      )
    ), length: _tabs.length,
  );
}
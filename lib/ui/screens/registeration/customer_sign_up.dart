import 'package:first_flutter_app/ui/widgerts/customized_button.dart';
import 'package:first_flutter_app/ui/widgerts/customized_dropdown_menu.dart';
import 'package:first_flutter_app/ui/widgerts/customized_text_filed.dart';
import 'package:flutter/material.dart';

class CustomerSignUp extends StatelessWidget {

  final nameInputController = TextEditingController();
  final emailInputController = TextEditingController();
  final passwordInputController = TextEditingController();
  final phoneNumberInputController = TextEditingController();

  static const options = [
    'Tripoli',
    'Benghazi',
    'misrate'
  ];

  final List<DropdownMenuItem<String>> _items = options.map(
    (String value) => DropdownMenuItem<String>(
      value: value,
      child: Text(value)
    )
  ).toList();

  @override
  Widget build(BuildContext context) => SingleChildScrollView(
      child: Center(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 30, horizontal: 0),
              child:  FlutterLogo(
                size: 100
              ),
            ),
            CustomizedTextField(title: 'Name', keyboardType: TextInputType.text, inputController: this.nameInputController),
            CustomizedTextField(title: 'E-mail', keyboardType: TextInputType.emailAddress, inputController: this.nameInputController),
            CustomizedTextField(title: 'Password', keyboardType: TextInputType.text, inputController: this.nameInputController, hide: true),
            CustomizedTextField(title: 'Phone Number', keyboardType: TextInputType.phone, inputController: this.nameInputController),
            CustomizedDropDownMenu(items: _items, hint: 'City'),
            CustomizedButton(title: 'Sign Up', padding: EdgeInsets.only(bottom: 10))
          ],
        ),
      )
    ),
  );
}
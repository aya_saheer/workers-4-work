import 'package:first_flutter_app/ui/widgerts/customized_button.dart';
import 'package:first_flutter_app/ui/widgerts/customized_input_with_icon.dart';
import 'package:first_flutter_app/ui/widgerts/customized_text_filed.dart';
import 'package:flutter/material.dart';

class SignIn extends StatelessWidget {
  final emailInputController = TextEditingController();
  final passwordInputController = TextEditingController();

  @override
  Widget build(BuildContext context) => Scaffold(
    body: SingleChildScrollView(
      child: Center(
        child: Padding(
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              FlutterLogo(
                size: 180
              ),
              CustomizedInputWithIcon(icon: Icons.person, title: 'E-mail', keyboardType: TextInputType.text, inputController: this.emailInputController),
              CustomizedInputWithIcon(icon: Icons.lock, title: 'Password', keyboardType: TextInputType.phone, inputController: this.passwordInputController, hide: true),
              CustomizedButton(title: 'Sign In', padding: EdgeInsets.only(left: 25, bottom: 10)),
              FlatButton(
                child: Text('Create Acount!'),
                onPressed: (){},
              )
            ],
          ),
        ),
      ),
    ),
  );
}
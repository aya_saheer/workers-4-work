import 'package:flutter/material.dart';

enum Priority {
  urgent,
  important,
  medium,
  useless,
}

class DummyTask {
  final String text;
  bool isChecked;
  final Priority priority;

  Color getColor() => {
        Priority.useless: Colors.grey,
        Priority.medium: Colors.green,
        Priority.important: Colors.yellow,
        Priority.urgent: Colors.red,
      }[this.priority];

  DummyTask({
    @required this.text,
    this.isChecked = false,
    this.priority = Priority.medium,
  });
}

class ShowWorkers extends StatelessWidget {

  final tasks = [
    DummyTask(text: "Dlkjdsfdsf sdf lkjaf lksaj flksjadf lkasjflkjsadf klasjdfkl asjdflk jasldkfjalskdfjlksjdflk jadsk lj"),
    DummyTask(text: "sdfxc sdf", isChecked: true),
    DummyTask(text: "qwe sdf", isChecked: true, priority: Priority.urgent),
    DummyTask(text: "asd sdf", priority: Priority.useless),
  ];

  @override
  Widget build(BuildContext context) => ListView(
          children: [
            ...tasks.map(
              (task) => ListTile(
                leading: CircleAvatar(
                  backgroundColor: Color(0xff345DA7),
                  
                  child: Text('img'),
                ),
                title: Text('Worker Name'),
                subtitle: Text('Specialization', style: TextStyle(color: Color(0xff345DA7))),
                trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Icon(
                        Icons.star,
                        size: 20,
                        color: Color(0xfffdd036)
                      ),
                      Icon(
                        Icons.star,
                        size: 20,
                        color: Color(0xfffdd036)
                      ),
                      Icon(
                        Icons.star,
                        size: 20,
                        color: Color(0xfffdd036)
                      ),
                      Icon(
                        Icons.star,
                        size: 20,
                        color: Colors.grey
                      )
                    ],
                  ),
                ),
              ),
          ],
        );
}
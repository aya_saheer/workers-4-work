
import 'package:first_flutter_app/ui/screens/main_screen.dart';
import 'package:first_flutter_app/ui/screens/registeration/sign_in.dart';
import 'package:first_flutter_app/ui/screens/registeration/sign_up.dart';
import 'package:flutter/material.dart';

void main(){
  runApp(AppBase());
}

class AppBase extends StatelessWidget {

  @override
  Widget build(BuildContext context) => MaterialApp(
    debugShowCheckedModeBanner: false,
    home: MainScreen()
  );
}